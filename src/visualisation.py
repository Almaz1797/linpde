import pylab
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl


def building_2d_plot(time, solution, borders):
    """Build a 2D plot in given time node.

    Keyword arguments:
    t -- time node
    solution -- a three-dimensional array of equation solutions
    borders -- boundaries of the area in format [[xmin, xmax], [ymin, ymax]]
    N -- ????
    """

    xmin, xmax = min(borders[0]), max(borders[0])
    ymin, ymax = min(borders[1]), max(borders[1])

    fig = plt.figure(figsize=(14, 6))

    N = 10
    bounds = np.linspace(np.min(solution[time]), np.max(solution[time]), N)
    cmap = mpl.cm.get_cmap('gnuplot2', N)

    ax = fig.add_subplot(111)
    cs = ax.contourf(solution[time], cmap=cmap, vmin=bounds[0], vmax=bounds[-1])
    cbar = fig.colorbar(cs, ax=ax, pad=0.01, ticks=bounds, extend='neither', orientation='vertical')

    ax.set_title(r'Visualisation')
    plt.ylabel(r'Y coordinate')
    plt.xlabel(r'X coordinate')

    x_label = np.arange(xmin, xmax, ((xmax - xmin) / 5))
    y_label = np.arange(ymin, ymax, ((ymax - ymin) / 5))

    ax.set_xticklabels(x_label, rotation=45)
    ax.set_yticklabels(y_label, rotation=45)

    ax.grid(True)

    plt.tight_layout()
    plt.show()


def building_3d_plot(time, solution, borders):
    """Build a 3D plot in given time node.

    Keyword arguments:
    time -- time node
    solution -- a three-dimensional array of equation solutions
    borders -- boundaries of the area in format [[xmin, xmax], [ymin, ymax]]
    """

    '''
    comments from me:
        this function works if a massive satisfies some dependency
        that's why I wrote: z = numpy.sin (2*x) * numpy.sin (y) / (x)
        for random massive a this function doesn't work
    '''

    xmin, xmax = min(borders[0]), max(borders[0])
    ymin, ymax = min(borders[1]), max(borders[1])

    x = np.arange(xmin, xmax, 0.1)
    y = np.arange(ymin, ymax, 0.1)
    x, y = np.meshgrid(x, y)

    z = solution[time]
    # z = np.sin(2 * x) * np.sin(y) / x

    fig = pylab.figure()
    axes = Axes3D(fig)
    axes.plot_surface(x, y, z, rstride=4, cstride=4, cmap=cm.jet)

    pylab.show()


