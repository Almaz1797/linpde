from model import Model
from medium import Medium
import methods

"""
dimension  - dimension of problem
matrices    - list of <dimensions> matrices 
density     - density of the medium
lame_coeffs - Lame coefficients
"""


class Solver:
    def __init__(self, dims, method_name):

        self.model = Model(dims, )
        self.medium = Medium()

        if method_name == "bicompact":
            self.method = methods.bicompact
        elif method_name == "fedorenko":
            self.method = methods.fedorenko
        elif method_name == "mccormack":
            self.method = methods.mccormack
        elif method_name == "splitting":
            self.method = methods.splitting
        elif method_name == "tvd":
            self.method = methods.tvd

    @staticmethod
    def solve(self):
        return 0
