import numpy as np


class Mesh2D:
    def __init__(self, x_start, x_end, y_start, y_end, x_num_points, y_num_points):
        x = np.linspace(x_start, x_end, x_num_points)
        y = np.linspace(y_start, y_end, y_num_points)
        self.__x_axis, self.__y_axis = np.meshgrid(x, y, sparse=True)

    def x_axis(self):
        return self.__x_axis

    def y_axis(self):
        return self.__y_axis
