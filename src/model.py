class Model:
    def __init__(self, dims, size, *args):
        self.__dimension = dims
        self.__size = size
        self.__matrices = []
        for matrix in args:
            self.__matrices.append(matrix)

    def set_size(self):
        print(self.__size)

    def dimension(self):
        return self.__dimension

    def size(self):
        return self.__size

    def matrix(self):
        return self.__matrices

