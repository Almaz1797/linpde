class Medium:
    def __init__(self, density, lame_lambda, lame_mu, mesh):
        self.__mesh = mesh

        self.__density = density
        self.__lame_lambda = lame_lambda
        self.__lame_mu = lame_mu

    def density(self, x, y):
        return self.__density(x, y)

    def lame_lambda(self, x, y):
        return self.__lame_lambda(x, y)

    def lame_mu(self, x, y):
        return self.__lame_mu(x, y)

    def youngs_modulus(self, x, y):
        return self.__lame_mu(x, y) * (3 * self.__lame_lambda(x, y) +
                                       2 * self.__lame_mu(x, y)) / (self.__lame_lambda(x, y) +
                                                                    self.__lame_mu(x, y))

    def poisson_ratio(self, x, y):
        return self.__lame_lambda(x, y) / (self.__lame_lambda(x, y) + self.__lame_mu(x, y)) / 2
