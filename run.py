import os
from src import solver
import numpy as np
import matplotlib.pyplot as plt

import src.solver

os.chdir("./tests/")

input_test_filename = "test_input.txt"

test_file_in = open(input_test_filename, 'r')
test_file_out = open("output_" + input_test_filename, 'w')

dimension = int(test_file_in.readline())

print()

my_solver = solver
